package com.dvt.undertheweather;

import com.dvt.undertheweather.models.WeatherModel;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Rofhiwa on 2017/05/03.
 */

public interface AppServices {

    @POST("weather")
    Call<WeatherModel> getTodayWeather(@Query("q") String cityName,
                                       @Query("units") String weatherUnits,
                                       @Query("APPID") String weatherApiKey);

}