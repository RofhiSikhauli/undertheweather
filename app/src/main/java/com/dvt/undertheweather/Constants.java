package com.dvt.undertheweather;

/**
 * Created by Rofhiwa on 2017/05/03.
 */

public class Constants {

    public final static String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    public final static String WEATHER_API_KEY = "11a5c79601193b8058b78ffc8e926904";

    public final static String WEATHER_UNITS = "metric";

    public final static char DEGREE_SYMBOL = '\u00B0';

    public final static int REQUEST_LOCATION_PERMISSION = 1;

}
