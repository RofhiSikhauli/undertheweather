package com.dvt.undertheweather;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.dvt.undertheweather.models.WeatherModel;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.providers.LocationManagerProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends Activity {

    TextView mTodayDate, mTodayMaxWeather, mTodayMinWeather, mAreaName, mProgressDescr;

    LinearLayout mProgress;

    LinearLayout mLinearlayout;

    Button mRetryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTodayDate           = (TextView) findViewById(R.id.todays_date);
        mTodayMaxWeather     = (TextView) findViewById(R.id.today_max_weather);
        mTodayMinWeather     = (TextView) findViewById(R.id.today_min_weather);
        mAreaName            = (TextView) findViewById(R.id.area_name);
        mLinearlayout        = (LinearLayout) findViewById(R.id.linearlayout);
        mProgress            = (LinearLayout) findViewById(R.id.progress);
        mProgressDescr       = (TextView) findViewById(R.id.progress_description);
        mRetryButton         = (Button) findViewById(R.id.retry_button);

        mProgressDescr.setText(getString(R.string.search_location));

        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrentCityWeather();
            }
        });

        setCurrentCityWeather();
    }

    /**
     * Get City name from coordinates
     * @param latitude
     * @param longitude
     * @return String
     */
    String getCity(double latitude, double longitude) {
        String cityName = null;
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        try{
            List<Address> addresses = gcd.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0)
                 cityName = addresses.get(0).getLocality() + ", " + addresses.get(0).getCountryName();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cityName;
    }

    /**
     * Get weather from current city
     */
    void setCurrentCityWeather() {

        /**
         * Request permission to access user location on users'
         * that are using device with Android OS 6+
         */
        requestUserPermission(Manifest.permission.ACCESS_FINE_LOCATION, Constants.REQUEST_LOCATION_PERMISSION);

        LocationManagerProvider locationManager = new LocationManagerProvider();
        locationManager.onProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isGpsAvailable = SmartLocation.with(this).location().state().isGpsAvailable();

        if (!isGpsAvailable) {
            locationManager.onProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }

        SmartLocation.with(this).location().start(new OnLocationUpdatedListener() {
            @Override
            public void onLocationUpdated(Location location) {

                if (location != null) {
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();
                    String cityName = getCity(latitude, longitude);

                    if (cityName != null) {
                        getWeather(cityName);
                    } else {
                        showRetryButton();
                    }
                } else {
                    showRetryButton();
                }
            }
        });
    }

    /**
     * Get Weather
     * @param cityName
     */
    void getWeather(final String cityName) {

        mProgressDescr.setText(getString(R.string.retrieve_weather));

        String formattedCity = cityName.split(",")[0] + ",ZA";

        Call<WeatherModel> serviceCall = ServiceUtil.getAppService().getTodayWeather(formattedCity,
                Constants.WEATHER_UNITS, Constants.WEATHER_API_KEY);

        serviceCall.enqueue(new Callback<WeatherModel>() {
            @Override
            public void onResponse(Call<WeatherModel> call, Response<WeatherModel> response) {

                if (response.body() != null) {
                    mAreaName.setText(cityName);
                    mTodayDate.setText(getTodayDate());
                    mTodayMaxWeather.setText("Max " + response.body().getMain().getTempMax() + Constants.DEGREE_SYMBOL + "C");
                    mTodayMinWeather.setText("Min " + response.body().getMain().getTempMin() + Constants.DEGREE_SYMBOL + "C");
                    mLinearlayout.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                    mRetryButton.setVisibility(View.GONE);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.unable_to_find_weather), Toast.LENGTH_LONG).show();
                    showRetryButton();
                }
            }

            @Override
            public void onFailure(Call<WeatherModel> call, Throwable t) {
                t.printStackTrace();
                showRetryButton();
            }
        });
    }

    /**
     * Show Retry button
     */
    void showRetryButton() {
        mProgress.setVisibility(View.GONE);
        mLinearlayout.setVisibility(View.GONE);
        mRetryButton.setVisibility(View.VISIBLE);
    }

    /**
     * Get today's date
     * @return String
     */
    String getTodayDate() {
        DateFormat dateFormat = new SimpleDateFormat("E, dd MMM yyyy", Locale.ENGLISH);
        Calendar calendar = Calendar.getInstance();
        return dateFormat.format(calendar.getTime());
    }

    /**
     * Request user permission on Android SDK 23+
     * @param permission
     */
    void requestUserPermission(String permission, int responseCode) {

        if (Build.VERSION.SDK_INT >= 23) {
            String[] REQUEST_PERMISSION = {permission};
            int b = ContextCompat.checkSelfPermission(this, permission);

            if (b != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, REQUEST_PERMISSION, responseCode);
                return;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_LOCATION_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setCurrentCityWeather();
                } else {
                    Toast.makeText(this, getString(R.string.location_permission_denied), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
