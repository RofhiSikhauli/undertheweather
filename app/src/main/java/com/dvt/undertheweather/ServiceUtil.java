package com.dvt.undertheweather;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rofhiwa on 2017/05/03.
 */
public class ServiceUtil {

    private static AppServices appServices;

    /**
     *
     * @return AppServices
     */
    public static AppServices getAppService() {
        if (appServices == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(logging);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
            appServices = retrofit.create(AppServices.class);
        }

        return appServices;
    }
}
